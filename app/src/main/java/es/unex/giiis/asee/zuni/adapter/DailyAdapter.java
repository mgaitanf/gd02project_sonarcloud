package es.unex.giiis.asee.zuni.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import es.unex.giiis.asee.zuni.api.daily.Datum;
import es.unex.giiis.asee.zuni.R;
import es.unex.giiis.asee.zuni.api.daily.Datum;
import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;

public class DailyAdapter extends RecyclerView.Adapter<es.unex.giiis.asee.zuni.adapter.DailyAdapter.MyViewHolder> {
    private List<DatumMinimal> mDataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
       public ImageView image_condition, image_sunrise, image_sunset;
       public TextView tv_day, tv_donde, tv_description, tv_sunrise, tv_sunset, tv_max,tv_min,tv_humidity,tv_ws;
        public View mView;

        public DatumMinimal mItem;

        public MyViewHolder(View v) {
            super(v);
            mView=v;

            image_condition = v.findViewById(R.id.image_condition);
            image_sunrise = v.findViewById(R.id.image_sunrise);
            image_sunset = v.findViewById(R.id.image_sunset);

            tv_day = v.findViewById(R.id.tv_day);
            tv_donde = v.findViewById(R.id.textViewDonde);
            tv_description = v.findViewById(R.id.tv_description);
            tv_sunrise = v.findViewById(R.id.tv_sunrise);
            tv_sunset = v.findViewById(R.id.tv_sunset);
            tv_max = v.findViewById(R.id.textMax);
            tv_min = v.findViewById(R.id.textMin);
            tv_humidity = v.findViewById(R.id.textH);
            tv_ws = v.findViewById(R.id.textWS);
        }
    }

    public DailyAdapter(ArrayList<DatumMinimal> myDataset) {
        mDataset = myDataset;
    }


    @Override
    public es.unex.giiis.asee.zuni.adapter.DailyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                  int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_days, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);

        if(position == 0){
            holder.tv_donde.setVisibility(View.VISIBLE);
            holder.tv_donde.setText(holder.mItem.getCity().concat(", ").concat(holder.mItem.getCountryCode()));
        }
        else{
            holder.tv_donde.setVisibility(View.GONE);
        }

        if(holder.mItem.getDescription().contains("thunder") || holder.mItem.getDescription().contains("storm") )
            holder.image_condition.setImageResource(R.drawable.tormenta);
        else if(holder.mItem.getDescription().contains("drizzle") || holder.mItem.getDescription().contains("rain") )
            holder.image_condition.setImageResource(R.drawable.lluvia);
        else if(holder.mItem.getDescription().contains("Snow") || holder.mItem.getDescription().contains("snow"))
                holder.image_condition.setImageResource(R.drawable.nieve);
        else if(holder.mItem.getDescription().contains("Clear") || holder.mItem.getDescription().contains("clear"))
                holder.image_condition.setImageResource(R.drawable.sol);
        else if(holder.mItem.getDescription().contains("Clouds") || holder.mItem.getDescription().contains("cloud"))
                holder.image_condition.setImageResource(R.drawable.nube);
        else if(holder.mItem.getDescription().contains("Mist") || holder.mItem.getDescription().contains("mist"))
            holder.image_condition.setImageResource(R.drawable.niebla);

        holder.tv_sunrise.setText(new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
                .format(new Date((holder.mItem.getSunriseTs()) * 1000l)));

        holder.tv_sunset.setText(new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
                .format(new Date((holder.mItem.getSunsetTs()) * 1000l)));

        holder.tv_day.setText(new SimpleDateFormat("E, dd MMM yyyy", Locale.ENGLISH)
                .format(new Date((holder.mItem.getTs()) * 1000l)));

        holder.tv_description.setText(holder.mItem.getDescription());

        holder.image_sunset.setImageResource(R.drawable.moonrise);
        holder.image_sunrise.setImageResource(R.drawable.sunrise);

        holder.tv_max.setText(Double.toString(holder.mItem.getMaxTemp()).concat(" ºC"));
        holder.tv_min.setText(Double.toString(holder.mItem.getMinTemp()).concat(" ºC"));

        holder.tv_humidity.setText(Double.toString(holder.mItem.getPop()).concat("%"));
        holder.tv_ws.setText(new DecimalFormat("###.##").format(holder.mItem.getWindSpeed()).concat(" m/s"));

    }

    @Override
    public int getItemCount() {

        return mDataset.size();
    }


    public void clear(){
        mDataset.clear();
        notifyDataSetChanged();
    }

    public void swap(List<DatumMinimal> dataset){

        mDataset = dataset;
        notifyDataSetChanged();
    }
}