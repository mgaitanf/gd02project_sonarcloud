package es.unex.giiis.asee.zuni.ui.ubicaciones;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository;

public class UbicacionesFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory{
    private final UbicacionRepository mRepository;

    public UbicacionesFragmentViewModelFactory(UbicacionRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass){
        return (T) new UbicacionesFragmentViewModel(mRepository);
    }
}
