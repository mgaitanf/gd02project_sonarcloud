package es.unex.giiis.asee.zuni.openweather;

import java.util.List;

import es.unex.giiis.asee.zuni.api.porhoras.Hourly;

public interface OnMeteoHorasLoadedListener {
    public void onMeteoHorasLoaded(List<Hourly> listHoras);
}
