package es.unex.giiis.asee.zuni.api.hourlies;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import es.unex.giiis.asee.zuni.api.daily.MainDaily;

@Entity(tableName = "hourly")
public class HourMinimal implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("main")
    @Expose
    private String main;

    @SerializedName("dt")
    @Expose
    private Long dt;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("pop")
    @Expose
    private double pop;

    @SerializedName("windSpeed")
    @Expose
    private Double windSpeed;

    @SerializedName("temp")
    @Expose
    private Double temp;

    @SerializedName("feelsLike")
    @Expose
    private Double feelsLike;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("countryCode")
    @Expose
    private String countryCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public Long getDt() {
        return dt;
    }

    public void setDt(Long dt) {
        this.dt = dt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPop() {
        return pop;
    }

    public void setPop(double pop) {
        this.pop = pop;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getFeelsLike() {
        return feelsLike;
    }

    public void setFeelsLike(Double feelsLike) {
        this.feelsLike = feelsLike;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Ignore
    public void initFromMainDaily(Hours md, int i){
        setCity(md.getCity().getName());
        setCountryCode(md.getCity().getCountry());

        setMain(md.getList().get(i).getWeather().get(0).getMain());
        setDescription(md.getList().get(i).getWeather().get(0).getDescription());


        setTemp(md.getList().get(i).getMain().getTemp());
        setFeelsLike(md.getList().get(i).getMain().getFeelsLike());
        setPop(md.getList().get(i).getPop());

        setDt(md.getList().get(i).getDt());

        setWindSpeed(md.getList().get(i).getWind().getSpeed());

    }
}
