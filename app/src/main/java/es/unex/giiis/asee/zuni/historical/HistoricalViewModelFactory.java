package es.unex.giiis.asee.zuni.historical;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class HistoricalViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final HistoricalRepository mRepository;

    public HistoricalViewModelFactory(HistoricalRepository repository){
        mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass){
        return (T) new HistoricalViewModel(mRepository);
    }
}
