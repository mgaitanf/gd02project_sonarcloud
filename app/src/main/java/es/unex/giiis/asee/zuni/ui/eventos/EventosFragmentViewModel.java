package es.unex.giiis.asee.zuni.ui.eventos;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.zuni.eventos.Evento;
import es.unex.giiis.asee.zuni.eventos.EventoRepository;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;

public class EventosFragmentViewModel extends ViewModel {

    private final EventoRepository mRepository;
    private final LiveData<List<Evento>> mEventos;
    private Evento evento = null;

    public EventosFragmentViewModel(EventoRepository repository){
        mRepository = repository;
        mEventos = mRepository.getEventos();
    }

    public LiveData<List<Evento>> getEventos() { return mEventos; }

    public void setEvento(Evento evento){
        this.evento = evento;
    }

    public Evento getEvento(){
        return this.evento;
    }

    /*
     * ----------------------------------------------------------------------------------------------
     * OPERACIONES DE LA BASE DE DATOS
     * ----------------------------------------------------------------------------------------------
     */


    /* GET EVENTO ------------------------------------------------------------------------------- */
    public Evento getEvento(long id){ return mRepository.getEvento(id); }


    /* DELETE EVENTO ---------------------------------------------------------------------------- */
    public void deleteEvento(Evento evento){ mRepository.deleteEvento(evento); }


    /* INSERT EVENTO ---------------------------------------------------------------------------- */
    public long insertEvento(Evento evento){
        return mRepository.insertEvento(evento);
    }


    /* UPDATE EVENTO ---------------------------------------------------------------------------- */
    public void updateEvento(Evento evento){
        mRepository.updateEvento(evento);
    }
}
