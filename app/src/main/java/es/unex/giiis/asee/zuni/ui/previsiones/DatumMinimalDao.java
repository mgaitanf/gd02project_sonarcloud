package es.unex.giiis.asee.zuni.ui.previsiones;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;
import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface DatumMinimalDao {

    @Insert(onConflict = REPLACE)
    void bulkInsert(List<DatumMinimal> datumMinimalList);

    @Query("SELECT * FROM datum WHERE city = :nombre")
    LiveData<List<DatumMinimal>> getDatumMinimalListByName(String nombre);

    @Query("DELETE FROM datum WHERE city = :nombre")
    int deleteDatumMinimalByName(String nombre);

    @Query("SELECT count(*) FROM datum WHERE city = :nombre")
    int getNumberMinimalDatumByName(String nombre);
}
