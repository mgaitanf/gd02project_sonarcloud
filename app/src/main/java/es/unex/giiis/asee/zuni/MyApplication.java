package es.unex.giiis.asee.zuni;

import android.app.Application;

import es.unex.giiis.asee.zuni.ui.detalles.DetallesContainer;
import es.unex.giiis.asee.zuni.ui.previsiones.PrevisionesContainer;

public class MyApplication extends Application {
    public PrevisionesContainer appContainerPrevisiones;
    public DetallesContainer appContainerDetalles;
    @Override
    public void onCreate() {
        super.onCreate();
        appContainerPrevisiones = new PrevisionesContainer(this);
        appContainerDetalles = new DetallesContainer(this);
    }
}
