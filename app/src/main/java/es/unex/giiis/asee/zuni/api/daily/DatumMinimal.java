package es.unex.giiis.asee.zuni.api.daily;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import es.unex.giiis.asee.zuni.api.hourlies.Main;

@Entity(tableName = "datum")
public class DatumMinimal  implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("sunriseTs")
    @Expose
    private long sunriseTs;

    @SerializedName("sunsetTs")
    @Expose
    private long sunsetTs;

    @SerializedName("ts")
    @Expose
    private long ts;

    @SerializedName("maxTemp")
    @Expose
    private double maxTemp;

    @SerializedName("minTemp")
    @Expose
    private double minTemp;

    @SerializedName("pop")
    @Expose
    private double pop;

    @SerializedName("windSpeed")
    @Expose
    private double windSpeed;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("countryCode")
    @Expose
    private String countryCode;

    @SerializedName("lat")
    @Expose
    private double lat;

    @SerializedName("lon")
    @Expose
    private double lon;

    @SerializedName("latlon")
    @Expose
    private String latlon;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getSunriseTs() {
        return sunriseTs;
    }

    public void setSunriseTs(long sunriseTs) {
        this.sunriseTs = sunriseTs;
    }

    public long getSunsetTs() {
        return sunsetTs;
    }

    public void setSunsetTs(long sunsetTs) {
        this.sunsetTs = sunsetTs;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public double getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(double maxTemp) {
        this.maxTemp = maxTemp;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(double minTemp) {
        this.minTemp = minTemp;
    }

    public double getPop() {
        return pop;
    }

    public void setPop(double pop) {
        this.pop = pop;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }


    public String getLatlon() {
        return latlon;
    }

    public void setLatlon(String latlon) {
        this.latlon = latlon;
    }

    @Ignore
    public void initFromMainDaily(MainDaily md, int i){
        setCity(md.getCityName());
        setCountryCode(md.getCountryCode());
        setLat(md.getLat());
        setLon(md.getLon());

        setDescription(md.getData().get(i).getWeather().getDescription());

        setMaxTemp(md.getData().get(i).getMaxTemp());

        setMinTemp(md.getData().get(i).getMinTemp());

        setPop(md.getData().get(i).getPop());

        setTs(md.getData().get(i).getTs());

        setSunriseTs(md.getData().get(i).getSunriseTs());
        setSunsetTs(md.getData().get(i).getSunsetTs());

        setWindSpeed(md.getData().get(i).getWindSpd());

    }

}
