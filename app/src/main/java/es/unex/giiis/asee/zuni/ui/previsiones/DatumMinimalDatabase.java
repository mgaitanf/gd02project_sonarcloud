package es.unex.giiis.asee.zuni.ui.previsiones;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;

@Database(entities = {DatumMinimal.class}, version = 1, exportSchema = false)
public abstract class DatumMinimalDatabase extends RoomDatabase {
    private static DatumMinimalDatabase INSTANCE;

    public synchronized static DatumMinimalDatabase getInstance(Context context) {
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context, DatumMinimalDatabase.class, "daily.db").build();
        }
        return INSTANCE;
    }

    public abstract DatumMinimalDao datumMinimalDao();

}
