package es.unex.giiis.asee.zuni.openweather;

import java.util.List;

import es.unex.giiis.asee.zuni.api.daily.Datum;
import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import es.unex.giiis.asee.zuni.api.daily.MainDaily;

public interface OnDailyLoadedListener {
    public void onDailyLoaded(List<DatumMinimal> list);
}
