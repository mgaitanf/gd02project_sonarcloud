package es.unex.giiis.asee.zuni;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.unex.giiis.asee.zuni.adapter.DailyAdapter;
import es.unex.giiis.asee.zuni.api.daily.Datum;
import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import es.unex.giiis.asee.zuni.api.daily.MainDaily;
import es.unex.giiis.asee.zuni.eventos.Evento;
import es.unex.giiis.asee.zuni.eventos.EventoRepository;
import es.unex.giiis.asee.zuni.eventos.db.EventoDatabase;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.openweather.DailyNetworkLoaderRunnable;
import es.unex.giiis.asee.zuni.ui.eventos.EventosFragmentViewModel;
import es.unex.giiis.asee.zuni.ui.eventos.EventosFragmentViewModelFactory;
import es.unex.giiis.asee.zuni.utils.DateRangeChecker;
import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;


public class DetallesEventoActivity extends AppCompatActivity {

    // Codigo para peticion de añadir evento
    private static final int EDIT_EVENTO_REQUEST = 2;

    private ProgressBar mProgressBar;
    private TextView descripcionTV;
    private TextView tituloTV;
    private TextView ubicacionTV;
    private TextView fechaTV;

    private TextView noPrevisionesTV;
    private TextView fechaDisponibleTV;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    DailyAdapter dAdapter;

    // ViewModel
    private EventosFragmentViewModel mViewModel;
    // Factory
    private EventosFragmentViewModelFactory mFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_evento);

        descripcionTV = findViewById(R.id.descripcionEventoTV);
        tituloTV = findViewById(R.id.tituloEventoTV);
        ubicacionTV = findViewById(R.id.ubicacionEventoTV);
        fechaTV = findViewById(R.id.fechaEventoTV);

        noPrevisionesTV = findViewById(R.id.noPrevisionesTV);
        fechaDisponibleTV = findViewById(R.id.fechaDisponibleEventoTV);
        mProgressBar = findViewById(R.id.progressBar4);
        Intent intent = getIntent();

        /* -------------------------------------------------------------------------------------- */
        /* VIEW MODEL --------------------------------------------------------------------------- */

        // Instanciar el factory
        mFactory = new EventosFragmentViewModelFactory(EventoRepository
                .getInstance(EventoDatabase.getInstance(this).getDao()));

        // Instanciar el ViewModel
        mViewModel = new ViewModelProvider(this, mFactory)
                .get(EventosFragmentViewModel.class);

        // Asignar el evento del ViewModel al recibido por intent
        mViewModel.setEvento(new Evento(intent));

        /* -------------------------------------------------------------------------------------- */
        /* -------------------------------------------------------------------------------------- */


        mRecyclerView = findViewById(R.id.listaPrevisionesEvento);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(DetallesEventoActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mostrarInfoEvento(mViewModel.getEvento());


        /* BOTON DE BORRAR EVENTO */
        FloatingActionButton deleteEventoFab = findViewById(R.id.deleteEventoFab);
        deleteEventoFab.setOnClickListener(v -> {
            setResult(RESULT_OK, intent);
            finish();
        });


        /* BOTON DE EDITAR EVENTO */
        FloatingActionButton editEventoFab = findViewById(R.id.editEventoFab);
        editEventoFab.setOnClickListener(v -> {
            Intent intent1 = new Intent(DetallesEventoActivity.this,
                    EditEventoActivity.class);

            Evento.packageIntent(intent1, mViewModel.getEvento());
            startActivityForResult(intent1, EDIT_EVENTO_REQUEST);
        });
    }


    /* CARGAR PREVISIONES EN EL ADAPTER --------------------------------------------------------- */
    private void cargarPrevisiones(Evento evento){
        mProgressBar.setVisibility(View.VISIBLE);
        dAdapter = new DailyAdapter(new ArrayList<>());
        AppExecutors.getInstance().networkIO().execute(new DailyNetworkLoaderRunnable(
                this::obtenerPrevisionEvento, evento.getLat(), evento.getLon()
        ));
        mRecyclerView.setAdapter(dAdapter);
    }

    public void obtenerPrevisionEvento(List<DatumMinimal> dataset){
        Date fechaEvento = mViewModel.getEvento().getFecha();
        Date now = new Date();
        now.setHours(0);
        now.setMinutes(0);
        now.setSeconds(0);

        int days = (int)( (fechaEvento.getTime() - now.getTime() + 1000) / (1000 * 60 * 60 * 24));

        List<DatumMinimal> newDataset = new ArrayList<>();
        if(days >= 0) {
            newDataset.add(dataset.get(days + 1));
        }

        mProgressBar.setVisibility(View.GONE);
        dAdapter.swap(newDataset);
    }


    /* MUESTRA LOS ATRIBUTOS DEL EVENTO EN LA PANTALLA ------------------------------------------ */
    private void mostrarInfoEvento(Evento evento){
        descripcionTV.setText(evento.getDescripcion());
        tituloTV.setText(evento.getTitulo());
        ubicacionTV.setText(evento.getUbicacion());
        fechaTV.setText(evento.getFecha().toString());

        // Se comprueba si la fecha del evento es en menos de 14 dias
        DateRangeChecker drc = new DateRangeChecker();
        if(drc.dateWithinDays(evento.getFecha(), 14)){
            noPrevisionesTV.setVisibility(View.INVISIBLE);
            fechaDisponibleTV.setVisibility(View.INVISIBLE);
            cargarPrevisiones(evento);
        } else {
            fechaDisponibleTV.setText(drc.substractDays(evento.getFecha(), 14).toString());
        }
    }

    /* SE ESPERA EL RESULTADO DE LA OPERACION EDIT_EVENTO_REQUEST ------------------------------- */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_EVENTO_REQUEST){
            if (resultCode == Activity.RESULT_OK){
                Evento eventoEditado = new Evento(data);
                eventoEditado.setId(mViewModel.getEvento().getId());

                /* ACTUALIZAR EVENTO EN LA BASE DE DATOS */
                AppExecutors.getInstance().diskIO().execute(() -> {
                    mViewModel.updateEvento(eventoEditado);
                    mViewModel.setEvento(eventoEditado);

                    // Se recarga el evento de esta clase para obtener el nuevo objeto
                    AppExecutors.getInstance().mainThread().execute(() -> {
                        if(dAdapter != null){
                            dAdapter.clear();
                        }
                        noPrevisionesTV.setVisibility(View.VISIBLE);
                        fechaDisponibleTV.setVisibility(View.VISIBLE);
                        mostrarInfoEvento(mViewModel.getEvento());
                    });
                });
            }
        }
    }
}