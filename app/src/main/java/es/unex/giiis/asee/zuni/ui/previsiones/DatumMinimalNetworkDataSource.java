package es.unex.giiis.asee.zuni.ui.previsiones;

import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.openweather.DailyNetworkLoaderRunnable;

public class DatumMinimalNetworkDataSource {
    private static final String LOG_TAG = DatumMinimalNetworkDataSource.class.getSimpleName();
    private static DatumMinimalNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<DatumMinimal[]> mDownloadedDatumMinimals;

    private DatumMinimalNetworkDataSource() {
        mDownloadedDatumMinimals = new MutableLiveData<>();
    }

    public synchronized static DatumMinimalNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new DatumMinimalNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<DatumMinimal[]> getCurrentDatumMinimals() {
        return mDownloadedDatumMinimals;
    }

    public void fetchDatumMinimal(String nombre, String countryCode) {
        Log.d(LOG_TAG, "Fetch repos started");
        // Get gata from network and pass it to LiveData
        AppExecutors.getInstance().networkIO().execute(new DailyNetworkLoaderRunnable(list -> mDownloadedDatumMinimals.postValue(list.toArray(new DatumMinimal[0])),nombre, countryCode));
    }
}