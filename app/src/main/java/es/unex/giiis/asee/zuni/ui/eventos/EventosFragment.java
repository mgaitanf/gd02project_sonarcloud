package es.unex.giiis.asee.zuni.ui.eventos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import es.unex.giiis.asee.zuni.AddEventoActivity;
import es.unex.giiis.asee.zuni.DetallesEventoActivity;
import es.unex.giiis.asee.zuni.R;
import es.unex.giiis.asee.zuni.eventos.Evento;
import es.unex.giiis.asee.zuni.eventos.EventoAdapter;
import es.unex.giiis.asee.zuni.eventos.EventoRepository;
import es.unex.giiis.asee.zuni.eventos.db.EventoDatabase;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;

public class EventosFragment extends Fragment {

    // Codigo para peticion de añadir evento
    private static final int ADD_EVENTO_REQUEST = 0;
    //  Codigo para peticion de eliminar evento
    private static final int DELETE_EVENTO_REQUEST = 1;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private EventoAdapter mAdapter;

    // ViewModel
    private EventosFragmentViewModel mViewModel;
    // Factory
    private EventosFragmentViewModelFactory mFactory;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_eventos, container, false);

        Log.d("Zuni","CARGANDO VISTA");

        /* FAB -> AÑADIR EVENTO ------------------------------------------------------------------*/
        FloatingActionButton fab = root.findViewById(R.id.addEventoFab);
        fab.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AddEventoActivity.class);
            startActivityForResult(intent, ADD_EVENTO_REQUEST);
        });


        /* -------------------------------------------------------------------------------------- */
        /* VIEW MODEL --------------------------------------------------------------------------- */

        // Instanciar el factory
        mFactory = new EventosFragmentViewModelFactory(EventoRepository
                .getInstance(EventoDatabase.getInstance(getActivity()).getDao()));

        // Instanciar el ViewModel
        mViewModel = new ViewModelProvider(this, mFactory)
                .get(EventosFragmentViewModel.class);

        // Observar los eventos del ViewModel
        mViewModel.getEventos().observe(this, eventos -> {
            mAdapter.swap(eventos);
        });

        /* -------------------------------------------------------------------------------------- */
        /* -------------------------------------------------------------------------------------- */


        mRecyclerView = root.findViewById(R.id.eventos_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        /* LISTENER DE CLICK EN LOS ITEMS DE LA LISTA ------------------------------------------- */
        mAdapter = new EventoAdapter(getActivity(), item -> {
            Log.d("Zuni","CLICK EVENTO");
            Intent intent = new Intent(getActivity(), DetallesEventoActivity.class);
            Evento.packageIntent(intent, item);
            startActivityForResult(intent, DELETE_EVENTO_REQUEST);
        });


        mRecyclerView.setAdapter(mAdapter);
        EventoDatabase.getInstance(getActivity());
        return root;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Snackbar.make(getView(), "Evento añadido", Snackbar.LENGTH_LONG);
        if (requestCode == ADD_EVENTO_REQUEST){
            if (resultCode == Activity.RESULT_OK){
                Evento evento = new Evento(data);
                /* INSERTAR EVENTO EN EL REPOSITORIO */
                AppExecutors.getInstance().diskIO().execute(() -> {
                    long id = mViewModel.insertEvento(evento);
                    evento.setId(id);
                    AppExecutors.getInstance().mainThread().execute(() -> mAdapter.add(evento));
                });
            }
        }
        else if (requestCode == DELETE_EVENTO_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Evento evento = new Evento(data);
                /* BORRAR EL EVENTO DEL REPOSITORIO */
                mViewModel.deleteEvento(evento);
                AppExecutors.getInstance().mainThread().execute(() -> mAdapter.remove(evento));
            }
        }
    }
}