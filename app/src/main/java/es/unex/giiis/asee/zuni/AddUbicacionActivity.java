package es.unex.giiis.asee.zuni;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import es.unex.giiis.asee.zuni.countrycodes.CountryCode;
import es.unex.giiis.asee.zuni.geocode.GeoCode;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.openweather.GeoCodeNetworkLoaderRunnable;
import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;

public class AddUbicacionActivity extends AppCompatActivity {

    private static final String TAG = "Zuni-AddUbicacion";

    private ProgressBar mProgressBar;
    private EditText mUbicacion;
    private TextView mTexto;
    private double lat;
    private double lon;
    private String mNombre, mCountryCode;
    private Spinner spinner3;
    private boolean correcto = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ubicacion);


        /* SE INICIALIZAN LOS INPUTS ------------------------------------------------------------ */

        mUbicacion = findViewById(R.id.ubicacionUbicacionInput);
        mTexto = findViewById(R.id.ubicacionTextoUbic);
        spinner3 = findViewById(R.id.spinner3);
        mProgressBar = findViewById(R.id.progressBar3);
        //Llamada a API para extraer códigos de país
        JsonReader reader = new JsonReader(new InputStreamReader(getResources().openRawResource(R.raw.country_codes)));
        List<CountryCode> countryCodes = Arrays.asList(new Gson().fromJson(reader, CountryCode[].class));
        ArrayAdapter<CountryCode> spinnerAdapter2 = new ArrayAdapter<>(this,R.layout.spinner_item,countryCodes);
        spinner3.setAdapter(spinnerAdapter2);
        spinner3.setSelection(208);



        /* Listener para el boton de CANCEL ----------------------------------------------------- */
        final Button cancelButton = findViewById(R.id.ubicacionCancel);
        cancelButton.setOnClickListener(v -> {
            Intent data = new Intent();
            setResult(RESULT_CANCELED);
            finish();
        });

        /* Listener para el boton de BUSCAR ------------------------------------------------------ */
        final Button buscarButton = findViewById(R.id.ubicacionBuscarButton);
        buscarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressBar.setVisibility(View.VISIBLE);

                AppExecutors.getInstance().networkIO().execute(new GeoCodeNetworkLoaderRunnable(
                        this::muestraUbicacion,mUbicacion.getText().toString().trim().replace(" ","%20"),spinner3.getSelectedItem().toString().substring(0,2)
                ));

            }

            private void muestraUbicacion(GeoCode geoCode) {

                mProgressBar.setVisibility(View.GONE);

                if(geoCode!=null) {
                    if (geoCode.getLatt() != null) {
                        if (geoCode.getLongt() != null) {
                            if (geoCode.getStandard().getCity() != null) {
                                if (!geoCode.getLatt().trim().equals("") && !geoCode.getLongt().trim().equals("") && !geoCode.getStandard().getCity().trim().equals("")) {

                                    lat = Double.parseDouble(geoCode.getLatt());
                                    lon = Double.parseDouble(geoCode.getLongt());
                                    mNombre = geoCode.getStandard().getCity();
                                    mCountryCode = geoCode.getStandard().getProv();
                                    mTexto.setTextSize(10);
                                    mTexto.setText(geoCode.getStandard().getCity().concat("\nlatt = ").concat(geoCode.getLatt()).concat(" , longt = ").concat(geoCode.getLongt()));

                                    correcto = true;
                                }else{
                                    mTexto.setText("No pudimos encontrarlo!");
                                    correcto = false;
                                }
                            }else{
                                mTexto.setText("No pudimos encontrarlo!");
                                correcto = false;
                            }
                        }else{
                            mTexto.setText("No pudimos encontrarlo!");
                            correcto = false;
                        }
                    }else{
                        mTexto.setText("No pudimos encontrarlo!");
                        correcto = false;
                    }
                }
                else{
                    mTexto.setText("No pudimos encontrarlo!");
                    correcto = false;
                }
            }

        });





        /* Listener para el boton de RESET ------------------------------------------------------ */
        final Button resetButton = findViewById(R.id.ubicacionReset);
        resetButton.setOnClickListener(v -> {
            mUbicacion.setText("");
            mTexto.setText("");
        });


        /* Listener para el boton de SUBMIT ----------------------------------------------------- */
        final Button submitButton = findViewById(R.id.ubicacionSubmit);
        submitButton.setOnClickListener(v -> {
            /* Obtener los datos del evento */
            if(correcto) {
                String ubicacion = mUbicacion.getText().toString().trim();

                String spinner = spinner3.getSelectedItem().toString();

                /* Empaquetar el evento en un intent */
                Intent data = new Intent();
                Log.e("AddUbi","CITY: "+ mNombre + ", CountryCode: " + mCountryCode);
                Ubicacion.packageIntent(data, mNombre, lat, lon, mCountryCode);

                Ubicacion ubicacionCreada = new Ubicacion(data);

                mUbicacion.setText(ubicacionCreada.toString());

                setResult(RESULT_OK, data);
                finish();
            }
        });

    }


    /*LOG --------------------------------------------------------------------------------------- */
    private void log(String msg) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i(TAG, msg);
    }
}