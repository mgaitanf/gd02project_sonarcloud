package es.unex.giiis.asee.zuni.ui.detalles;

import android.content.Context;

public class DetallesContainer {
    private HourMinimalDatabase database;
    private HourMinimalNetworkDataSource networkDataSource;
    public HourMinimalRepository repository;
    public DetallesFragmentViewModelFactory factory;

    public DetallesContainer(Context context){
        database = HourMinimalDatabase.getInstance(context);
        networkDataSource = HourMinimalNetworkDataSource.getInstance();
        repository = HourMinimalRepository.getInstance(database.hourMinimalDao(), networkDataSource);
        factory = new DetallesFragmentViewModelFactory(repository);
    }
}