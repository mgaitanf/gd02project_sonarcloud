package es.unex.giiis.asee.zuni.openweather;

import java.util.List;

import es.unex.giiis.asee.zuni.api.hourlies.HourMinimal;

public interface OnHourLoadedListener {
    public void onHourlyLoaded(List<HourMinimal> list);
}
