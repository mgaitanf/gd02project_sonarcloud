package es.unex.giiis.asee.zuni.ui.previsiones;

import android.util.Log;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;

/**
 * Handles data operations in Sunshine. Acts as a mediator between {@link DatumMinimalNetworkDataSource}
 * and {@link DatumMinimalDao}
 */
public class DatumMinimalRepository {
    private static final String LOG_TAG = DatumMinimalRepository.class.getSimpleName();

    // For Singleton instantiation
    private static DatumMinimalRepository sInstance;
    private final DatumMinimalDao mDatumMinimalDao;
    private final DatumMinimalNetworkDataSource mDatumMinimalNetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final MutableLiveData<String> userFilterLiveData = new MutableLiveData<>();
    private final Map<String, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 30000;

    private DatumMinimalRepository(DatumMinimalDao datumMinimalDao, DatumMinimalNetworkDataSource datumMinimalNetworkDataSource) {
        mDatumMinimalDao = datumMinimalDao;
        mDatumMinimalNetworkDataSource = datumMinimalNetworkDataSource;
        // LiveData that fetches repos from network
        LiveData<DatumMinimal[]> networkData = mDatumMinimalNetworkDataSource.getCurrentDatumMinimals();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(newDatumMinimalsFromNetwork -> {
            mExecutors.diskIO().execute(() -> {
                // Deleting cached repos of user
                if (newDatumMinimalsFromNetwork.length > 0){
                    datumMinimalDao.deleteDatumMinimalByName(newDatumMinimalsFromNetwork[0].getCity());
                }
                // Insert our new repos into local database
                datumMinimalDao.bulkInsert(Arrays.asList(newDatumMinimalsFromNetwork));
                Log.d(LOG_TAG, "New values inserted in Room");
            });
        });
    }

    public synchronized static DatumMinimalRepository getInstance(DatumMinimalDao dao, DatumMinimalNetworkDataSource nds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new DatumMinimalRepository(dao, nds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void setName(String nombre, String countryCode){
        // Set value to MutableLiveData in order to filter getCurrentRepos LiveData
        userFilterLiveData.setValue(nombre);
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeeded(nombre)) {
                doFetchDatumMinimals(nombre, countryCode);
            }
        });
    }

    public void doFetchDatumMinimals(String name, String countryCode){
        Log.d(LOG_TAG, "Fetching Repos from Github");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mDatumMinimalDao.deleteDatumMinimalByName(name);
            mDatumMinimalNetworkDataSource.fetchDatumMinimal(name,countryCode);
            lastUpdateTimeMillisMap.put(name, System.currentTimeMillis());
        });
    }

    public LiveData<List<DatumMinimal>> getCurrentDatumMinimals() {
        // Return LiveData from Room. Use Transformation to get owner
        return Transformations.switchMap(userFilterLiveData, mDatumMinimalDao::getDatumMinimalListByName);
    }

    private boolean isFetchNeeded(String nombre) {
        Long lastFetchTimeMillis = lastUpdateTimeMillisMap.get(nombre);
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;

        // Implement cache policy: When time has passed or no repos in cache
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS || mDatumMinimalDao.getNumberMinimalDatumByName(nombre) == 0;
    }
}