package es.unex.giiis.asee.zuni.ui.previsiones;

import android.content.Context;

public class PrevisionesContainer {
    private DatumMinimalDatabase database;
    private DatumMinimalNetworkDataSource networkDataSource;
    public DatumMinimalRepository repository;
    public PrevisionesFragmentViewModelFactory factory;

    public PrevisionesContainer(Context context){
        database = DatumMinimalDatabase.getInstance(context);
        networkDataSource = DatumMinimalNetworkDataSource.getInstance();
        repository = DatumMinimalRepository.getInstance(database.datumMinimalDao(), networkDataSource);
        factory = new PrevisionesFragmentViewModelFactory(repository);
    }
}
