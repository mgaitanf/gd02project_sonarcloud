package es.unex.giiis.asee.zuni.openweather;

import es.unex.giiis.asee.zuni.geocode.GeoCode;

public interface OnGeoCodeLoadedListener {
    public void onGeoCodeLoaded(GeoCode geoCode);
}
