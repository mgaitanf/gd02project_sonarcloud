package es.unex.giiis.asee.zuni.openweather;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import es.unex.giiis.asee.zuni.api.daily.MainDaily;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class DailyNetworkLoaderRunnable implements Runnable {

    private final OnDailyLoadedListener mOnDailyLoadedListener;

    private double longt, latt;
    private String city, country;
    public DailyNetworkLoaderRunnable(OnDailyLoadedListener onDailyLoadedListener, double latt, double longt){
        mOnDailyLoadedListener= onDailyLoadedListener;
        this.latt=latt;
        this.longt=longt;
        city=null;
        country=null;
    }

    public DailyNetworkLoaderRunnable(OnDailyLoadedListener onDailyLoadedListener,String city, String country){
        mOnDailyLoadedListener= onDailyLoadedListener;

        this.city=city;
        this.country=country;
    }

    @Override
    public void run() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.weatherbit.io/v2.0/forecast/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenWeatherMapService service = retrofit.create(OpenWeatherMapService.class);
        try {

            if(city!=null){

                if(!city.equals("")){
                    MainDaily mainDaily = service.listDailyCity(city,country,"5a03135617514e24ad8e588aca207439").execute().body();
                    if(mainDaily!=null) {
                        if(mainDaily.getData()!=null) {
                            List<DatumMinimal> listDatumMinimal = new ArrayList<>();
                            for(int i=0;i<mainDaily.getData().size();i++){
                                DatumMinimal d = new DatumMinimal();
                                d.initFromMainDaily(mainDaily,i);
                                listDatumMinimal.add(d);
                            }
                            AppExecutors.getInstance().mainThread().execute(() -> mOnDailyLoadedListener.onDailyLoaded(listDatumMinimal));
                        }
                    }
                }
            }
            else{

                MainDaily mainDaily = service.listDaily(Double.toString(latt),Double.toString(longt),"5a03135617514e24ad8e588aca207439").execute().body();
                if(mainDaily!=null) {
                    if(mainDaily.getData()!=null) {
                        List<DatumMinimal> listDatumMinimal = new ArrayList<>();
                        for(int i=0;i<mainDaily.getData().size();i++){
                            DatumMinimal d = new DatumMinimal();
                            d.initFromMainDaily(mainDaily,i);
                            listDatumMinimal.add(d);
                        }

                        AppExecutors.getInstance().mainThread().execute(() -> mOnDailyLoadedListener.onDailyLoaded(listDatumMinimal));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
