package es.unex.giiis.asee.zuni.ui.detalles;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import es.unex.giiis.asee.zuni.api.hourlies.HourMinimal;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;

import es.unex.giiis.asee.zuni.openweather.HourNetworkLoaderRunnable;


public class HourMinimalNetworkDataSource {
    private static final String LOG_TAG = HourMinimalNetworkDataSource.class.getSimpleName();
    private static HourMinimalNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<HourMinimal[]> mDownloadedHourMinimals;

    private HourMinimalNetworkDataSource() {
        mDownloadedHourMinimals = new MutableLiveData<>();
    }

    public synchronized static HourMinimalNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new HourMinimalNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<HourMinimal[]> getCurrentHourMinimals() {
        return mDownloadedHourMinimals;
    }

    public void fetchHourMinimal(String nombre, String countryCode) {
        Log.d(LOG_TAG, "Fetch repos started");
        // Get gata from network and pass it to LiveData
        AppExecutors.getInstance().networkIO().execute(new HourNetworkLoaderRunnable(list -> mDownloadedHourMinimals.postValue(list.toArray(new HourMinimal[0])),nombre, countryCode));
    }
}
