package es.unex.giiis.asee.zuni.ui.eventos;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.zuni.eventos.EventoRepository;

public class EventosFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final EventoRepository mRepository;

    public EventosFragmentViewModelFactory(EventoRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass){
        return (T) new EventosFragmentViewModel(mRepository);
    }
}
