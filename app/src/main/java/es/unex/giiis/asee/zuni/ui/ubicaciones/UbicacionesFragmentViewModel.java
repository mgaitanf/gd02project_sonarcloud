package es.unex.giiis.asee.zuni.ui.ubicaciones;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository;


public class UbicacionesFragmentViewModel extends ViewModel {
    private final UbicacionRepository mRepository;
    private final LiveData<List<Ubicacion>> mUbicaciones;


    public UbicacionesFragmentViewModel(UbicacionRepository repository){
        mRepository = repository;
        mUbicaciones = mRepository.getUbicaciones();
    }

    public LiveData<List<Ubicacion>> getUbicaciones() { return mUbicaciones; }

    /*
     * ----------------------------------------------------------------------------------------------
     * OPERACIONES DE LA BASE DE DATOS
     * ----------------------------------------------------------------------------------------------
     */


    /* GET UBICACION ------------------------------------------------------------------------------- */
    public Ubicacion getUbicacion(long id){ return mRepository.getUbicacion(id); }


    /* DELETE UBICACION ---------------------------------------------------------------------------- */
    public void deleteUbicacion(Ubicacion ubicacion){ mRepository.deleteUbicacion(ubicacion); }


    /* INSERT UBICACION ---------------------------------------------------------------------------- */
    public long insertUbicacion(Ubicacion ubicacion){
        return mRepository.insertUbicacion(ubicacion);
    }


    /* UPDATE UBICACION ---------------------------------------------------------------------------- */
    public void updateUbicacion(Ubicacion ubicacion){
        mRepository.updateUbicacion(ubicacion);
    }
}


