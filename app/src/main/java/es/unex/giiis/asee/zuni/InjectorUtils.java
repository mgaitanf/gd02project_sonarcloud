package es.unex.giiis.asee.zuni;

import android.content.Context;

import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import es.unex.giiis.asee.zuni.ui.previsiones.DatumMinimalDatabase;
import es.unex.giiis.asee.zuni.ui.previsiones.DatumMinimalNetworkDataSource;
import es.unex.giiis.asee.zuni.ui.previsiones.DatumMinimalRepository;
import es.unex.giiis.asee.zuni.ui.previsiones.PrevisionesFragmentViewModelFactory;

public class InjectorUtils {
    public static DatumMinimalRepository provideRepository(Context context) {
        DatumMinimalDatabase database = DatumMinimalDatabase.getInstance(context.getApplicationContext());
        DatumMinimalNetworkDataSource networkDataSource = DatumMinimalNetworkDataSource.getInstance();
        return DatumMinimalRepository.getInstance(database.datumMinimalDao(), networkDataSource);
    }

    public static PrevisionesFragmentViewModelFactory providePrevisionesFragmentViewModelFactory(Context context) {
        DatumMinimalRepository repository = provideRepository(context.getApplicationContext());
        return new PrevisionesFragmentViewModelFactory(repository);
    }
}
