package es.unex.giiis.asee.zuni.ui.detalles;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import es.unex.giiis.asee.zuni.api.hourlies.HourMinimal;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;


/**
 * Handles data operations in Sunshine. Acts as a mediator between {@link HourMinimalNetworkDataSource}
 * and {@link HourMinimalDao}
 */
public class HourMinimalRepository {
    private static final String LOG_TAG = HourMinimalRepository.class.getSimpleName();

    // For Singleton instantiation
    private static HourMinimalRepository sInstance;
    private final HourMinimalDao mHourMinimalDao;
    private final HourMinimalNetworkDataSource mHourMinimalNetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final MutableLiveData<String> userFilterLiveData = new MutableLiveData<>();
    private final Map<String, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 30000;

    private HourMinimalRepository(HourMinimalDao hourMinimalDao, HourMinimalNetworkDataSource hourMinimalNetworkDataSource) {
        mHourMinimalDao = hourMinimalDao;
        mHourMinimalNetworkDataSource = hourMinimalNetworkDataSource;
        // LiveData that fetches repos from network
        LiveData<HourMinimal[]> networkData = mHourMinimalNetworkDataSource.getCurrentHourMinimals();
        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        networkData.observeForever(newHourMinimalsFromNetwork -> {
            mExecutors.diskIO().execute(() -> {
                // Deleting cached repos of user
                if (newHourMinimalsFromNetwork.length > 0){
                    hourMinimalDao.deleteHourMinimalByName(newHourMinimalsFromNetwork[0].getCity());
                }
                // Insert our new repos into local database
                hourMinimalDao.bulkInsert(Arrays.asList(newHourMinimalsFromNetwork));
                Log.d(LOG_TAG, "New values inserted in Room");
            });
        });
    }

    public synchronized static HourMinimalRepository getInstance(HourMinimalDao dao, HourMinimalNetworkDataSource nds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new HourMinimalRepository(dao, nds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void setName(String nombre, String countryCode){
        // Set value to MutableLiveData in order to filter getCurrentRepos LiveData
        userFilterLiveData.setValue(nombre);
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeeded(nombre)) {
                doFetchHourMinimals(nombre, countryCode);
            }
        });
    }

    public void doFetchHourMinimals(String name, String countryCode){
        Log.d(LOG_TAG, "Fetching Repos from Github");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mHourMinimalDao.deleteHourMinimalByName(name);
            mHourMinimalNetworkDataSource.fetchHourMinimal(name,countryCode);
            lastUpdateTimeMillisMap.put(name, System.currentTimeMillis());
        });
    }

    public LiveData<List<HourMinimal>> getCurrentHourMinimals() {
        // Return LiveData from Room. Use Transformation to get owner
        return Transformations.switchMap(userFilterLiveData, mHourMinimalDao::getHourMinimalListByName);
    }

    private boolean isFetchNeeded(String nombre) {
        Long lastFetchTimeMillis = lastUpdateTimeMillisMap.get(nombre);
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;

        // Implement cache policy: When time has passed or no repos in cache
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS || mHourMinimalDao.getNumberMinimalHourByName(nombre) == 0;
    }
}
