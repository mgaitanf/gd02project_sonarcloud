package es.unex.giiis.asee.zuni;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.junit.Test;

import java.util.Date;

import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import es.unex.giiis.asee.zuni.eventos.Evento;

import static org.junit.Assert.assertEquals;
public class CU04 {
    private static final double DELTA = 1e-15;

    @Test
    public void shouldDatumMinimalArqSoftware(){

        // Primera prueba

        DatumMinimal datumMinimal = new DatumMinimal();
        // id, description, sunriseTs, sunsetTs, ts
        datumMinimal.setId(0);
        datumMinimal.setDescription("Overcast clouds");
        datumMinimal.setSunriseTs(12345678);
        datumMinimal.setSunsetTs(9876543);
        datumMinimal.setTs(65434567);

        assertEquals(datumMinimal.getId(),0);
        assertEquals(datumMinimal.getDescription(),"Overcast clouds");
        assertEquals(datumMinimal.getSunriseTs(),12345678);
        assertEquals(datumMinimal.getSunsetTs(),9876543);
        assertEquals(datumMinimal.getTs(),65434567);
    }

    @Test
    public void shouldDatumMinimalSenior(){
        DatumMinimal datumMinimal = new DatumMinimal();

        /* maxTemp, minTemp, pop, windspeed, city */
        double maxT = 40;
        double minT = 10;
        double pop = 45;
        double windS = 20;
        String city = "Madrid";

        datumMinimal.setMaxTemp(maxT);
        datumMinimal.setMinTemp(minT);
        datumMinimal.setPop(pop);
        datumMinimal.setWindSpeed(windS);
        datumMinimal.setCity(city);

        assertEquals(datumMinimal.getMaxTemp(), maxT, 1);
        assertEquals(datumMinimal.getMinTemp(), minT, 1);
        assertEquals(datumMinimal.getPop(), pop, 1);
        assertEquals(datumMinimal.getWindSpeed(), windS, 1);
        assertEquals(datumMinimal.getCity(), city);
    }


    @Test
    public void shouldDatumMinimalJunior() {
        //Instanciar la clase DatumMinimal
        DatumMinimal dm = new DatumMinimal();

        //Declarar los valores que se le van a insertar
        String countryCode = "ES";
        double lat = 1.23;
        double lon = 9.87;
        String latlon = "aasd";

        //Insertar los valores usando setters
        dm.setCountryCode(countryCode);
        dm.setLat(lat);
        dm.setLon(lon);
        dm.setLatlon(latlon);

        //Probar los getters
        assertEquals(countryCode, dm.getCountryCode());
        assertEquals(lat, dm.getLat(), DELTA);
        assertEquals(lon, dm.getLon(), DELTA);
        assertEquals(latlon, dm.getLatlon());
    }
}
