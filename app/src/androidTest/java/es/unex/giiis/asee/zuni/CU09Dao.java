package es.unex.giiis.asee.zuni;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import es.unex.giiis.asee.zuni.eventos.Evento;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import es.unex.giiis.asee.zuni.eventos.db.EventoDao;
import es.unex.giiis.asee.zuni.eventos.db.EventoDatabase;

@RunWith(AndroidJUnit4.class)
public class CU09Dao {
    private EventoDao eventoDao;
    private EventoDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db =Room.inMemoryDatabaseBuilder(context, EventoDatabase.class).build();
        eventoDao = db.getDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void shoudlEventoGetterSetter(){
        //Instanciamos la clase Evento
        Evento ev = new Evento(0, "", "", "", "BAJA", "",0.0, 0.0);

        //Definimos los valores a insertar
        long id = 123;
        String titulo = "titulo de prueba";
        String descripcion = "descripcion de prueba";
        Date fecha = new Date();
        Evento.Alerta alerta = Evento.Alerta.BAJA;
        String ubicacion = "Badajoz";
        Double lat = 12.3;
        Double lon = 6.54;


        //Probamos insertar los atributos con setters
        ev.setId(id);
        ev.setTitulo(titulo);
        ev.setDescripcion(descripcion);
        ev.setFecha(fecha);
        ev.setAlerta(alerta);
        ev.setUbicacion(ubicacion);
        ev.setLat(lat);
        ev.setLon(lon);


        //Probamos recuperar los valores con getters
        assertEquals(id, ev.getId());
        assertEquals(titulo, ev.getTitulo());
        assertEquals(descripcion, ev.getDescripcion());
        assertEquals(fecha, ev.getFecha());
        assertEquals(alerta, ev.getAlerta());
        assertEquals(ubicacion, ev.getUbicacion());
        assertEquals(lat, ev.getLat());
        assertEquals(lon, ev.getLon());
    }


    @Test
    public void shouldEventoInstance() {
        //Definimos los valores a insertar
        long id = 123;
        String titulo = "titulo de prueba";
        String descripcion = "descripcion de prueba";
        Date fecha = new Date();
        Evento.Alerta alerta = Evento.Alerta.BAJA;
        String ubicacion = "Badajoz";
        Double lat = 12.3;
        Double lon = 6.54;


        //Instanciamos la clase Evento
        Evento ev = new Evento(id, titulo, descripcion, fecha, alerta, ubicacion,lat, lon);


        //Probamos recuperar los valores con getters
        assertEquals(id, ev.getId());
        assertEquals(titulo, ev.getTitulo());
        assertEquals(descripcion, ev.getDescripcion());
        assertEquals(fecha, ev.getFecha());
        assertEquals(alerta, ev.getAlerta());
        assertEquals(ubicacion, ev.getUbicacion());
        assertEquals(lat, ev.getLat());
        assertEquals(lon, ev.getLon());
    }

    @Test
    public void shouldEventoInsert() throws Exception {

        Evento e1 = eventoDao.getEvento(123);

        assertEquals(e1,null);

        //Definimos los valores a insertar
        long id = 123;
        String titulo = "titulo de prueba";
        String descripcion = "descripcion de prueba";
        Date fecha = new Date();
        Evento.Alerta alerta = Evento.Alerta.BAJA;
        String ubicacion = "Badajoz";
        Double lat = 12.3;
        Double lon = 6.54;


        //Instanciamos la clase Evento
        Evento evento = new Evento(id, titulo, descripcion, fecha, alerta, ubicacion,lat, lon);
        eventoDao.insert(evento);

        Evento e2 = eventoDao.getEvento(123);

        assertEquals(e2.getId(),123);

    }
}
