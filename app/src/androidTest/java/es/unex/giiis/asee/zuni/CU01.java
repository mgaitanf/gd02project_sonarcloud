package es.unex.giiis.asee.zuni;


import android.view.Gravity;

import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.DrawerMatchers;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CU01 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldAddUbicacion (){
        String testString = "Badajoz";


        //Partimos de que no hay ninguna ubicacion añadida en la base de datos


        /* NAVEGACION --------------------------------------------------------------------------- */

        // Abrir drawer
        onView(withId(R.id.drawer_layout)).check(matches(DrawerMatchers.isClosed(Gravity.LEFT))).perform(DrawerActions.open(), closeSoftKeyboard());
        // Navegar a previsiones por dia
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_ubicaciones), closeSoftKeyboard());
        // Pausa para cargar la vista
        try {
            Thread.sleep(500);
        } catch (InterruptedException ignored) { }


        /* ABRIR ACTIVIDAD --------------------- */
        //Hacer click en el boto de añadir ubicacion
        onView(withId(R.id.addUbicaciones)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignored) { }



        /* AÑADIR UBICACION ------------------- */
        //Insertar nombre de la ciudad
        onView(withId(R.id.ubicacionUbicacionInput)).perform(typeText(testString), closeSoftKeyboard());

        //Realizar 3 clicks seguidos al boton de buscar
        for (int i = 0; i < 3; i++){
            //Hacer click el boton de buscar
            onView(withId(R.id.ubicacionBuscarButton)).perform(click());
            try {
                Thread.sleep(200);
            } catch (InterruptedException ignored) { }
        }

        //Esperar a obtener el resultado de la API
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ignored) { }


        //Hacer click en añadir ubicacion
        onView(withId(R.id.ubicacionSubmit)).perform(click());


        //Esperar a que se vuelva al fragment
        try {
            Thread.sleep(500);
        } catch (InterruptedException ignored) { }




        /* COMPROBAR QUE SE HA AÑADIDO LA UBICACION ------------------- */

        // Comprobar si hay algun elemento en la lista
        onView(withId(R.id.ubicaciones_recycler_view)).check(matches(hasDescendant(withId(R.id.card_view))));
    }

}

