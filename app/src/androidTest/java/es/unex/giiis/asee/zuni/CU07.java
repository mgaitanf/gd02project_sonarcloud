package es.unex.giiis.asee.zuni;
import android.view.Gravity;

import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.DrawerMatchers;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/* ============================================================================================== */
/* CU07 : Consultar lista de ubicaciones  ======================================================= */
/* ============================================================================================== */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CU07 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldCheckLocationsList() throws InterruptedException {

        String testString = "Valencia";

        /* NAVEGACION --------------------------------------------------------------------------- */

        // Abrir drawer
        onView(withId(R.id.drawer_layout)).check(matches(DrawerMatchers.isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open(), closeSoftKeyboard());
        // Navegar a ubicaciones
        onView(withId(R.id.nav_view)).perform(NavigationViewActions
                .navigateTo(R.id.nav_ubicaciones), closeSoftKeyboard());
        // Pausa para cargar la vista
        Thread.sleep(500);

        /* PANTALLA UBICACIONES ----------------------------------------------------------------- */

        // Click añadir ubicacion
        onView(withId(R.id.addUbicaciones)).perform(click());

        /* PANTALLA AÑADIR UBICACION ------------------------------------------------------------ */

        // Escribir nombre de la ubicacion
        onView(withId(R.id.ubicacionUbicacionInput)).perform(typeText(testString), closeSoftKeyboard());
        // Click en buscar
        onView(withId(R.id.ubicacionBuscarButton)).perform(click(), closeSoftKeyboard());
        Thread.sleep(4000);
        // Click en guardar
        onView(withId(R.id.ubicacionSubmit)).perform(click());
        Thread.sleep(1000);

        /* PANTALLA UBICACIONES ----------------------------------------------------------------- */

        // Comprobamos que hay alguna ubicacion en la lista
        onView(withId(R.id.ubicaciones_recycler_view)).check(matches(
                hasDescendant(withId(R.id.ubicacionUbicacion))
        ));
    }
}
