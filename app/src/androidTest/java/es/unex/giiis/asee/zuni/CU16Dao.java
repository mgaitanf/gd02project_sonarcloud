package es.unex.giiis.asee.zuni;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import es.unex.giiis.asee.zuni.eventos.Evento;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import es.unex.giiis.asee.zuni.eventos.db.EventoDao;
import es.unex.giiis.asee.zuni.eventos.db.EventoDatabase;

@RunWith(AndroidJUnit4.class)
public class CU16Dao {
    private EventoDao eventoDao;
    private EventoDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db =Room.inMemoryDatabaseBuilder(context, EventoDatabase.class).build();
        eventoDao = db.getDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void shouldUpdateDao() throws Exception {
        Date fecha = new Date();

        Evento evento = new Evento(999, "Titulo", "Desc", fecha, Evento.Alerta.ALTA, "Madrid", 40.9, 40.1);
        eventoDao.insert(evento);

        evento = eventoDao.getEvento(999);
        evento.setTitulo("Este titulo es nuevo");

        eventoDao.update(evento);

        Evento e1 = eventoDao.getEvento(999);

        assertEquals(e1.getTitulo(),evento.getTitulo());

    }
}
