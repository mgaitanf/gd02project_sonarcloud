package es.unex.giiis.asee.zuni;

import android.view.Gravity;

//import androidx.test.espresso.contrib.DrawerActions;
//import androidx.test.espresso.contrib.DrawerMatchers;
//import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.DrawerMatchers;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
//import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;



@RunWith(AndroidJUnit4.class)
@LargeTest
public class CU08 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testing() throws InterruptedException {
        String testString = "Barcelona";

        /* NAVEGACION --------------------------------------------------------------------------- */

        // Abrir drawer
        onView(withId(R.id.drawer_layout)).check(matches(DrawerMatchers.isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open());
        // Navegar a historico
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_historical));
        // Pausa para cargar la vista
        Thread.sleep(500);

        /* PANTALLA HISTORICO --------------------------------------------------------------------- */

        onView(withId(R.id.button2)).perform(click(), closeSoftKeyboard());

        Thread.sleep(2000);

        // obtenemos el numero de elementos
        RecyclerViewItemCountAssertion r = new RecyclerViewItemCountAssertion(0);
        onView(withId(R.id.listHistorical)).check(r);
        int num = r.count;




        // Click añadir historico
        onView(withId(R.id.addHistorical)).perform(click());
        // Escribir nombre de la ciudad
        //onView(withId(R.id.et_city)).perform(typeText(testString), closeSoftKeyboard());
        // Click en buscar
        onView(withId(R.id.buttonSearch2)).perform(click());

        Thread.sleep(10000);

        onView(withId(R.id.buttonSave1)).perform(click());

        Thread.sleep(500);

        onView(withId(R.id.button2)).perform(click());

        Thread.sleep(1000);

        r.setExpected(num+1);
        onView(withId(R.id.listHistorical)).check(r);
    }
}
