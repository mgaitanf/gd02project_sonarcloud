package es.unex.giiis.asee.zuni;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.io.IOException;


import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDao;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDatabase;


@RunWith(AndroidJUnit4.class)
public class CU01Dao {

    private UbicacionDao ubicacionDao;
    private UbicacionDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db =Room.inMemoryDatabaseBuilder(context, UbicacionDatabase.class).build();
        ubicacionDao = db.getDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void shouldCreateInstance(){

        // Se definen los valores de los atributos
        int id = 999;
        String ubi = "Madrid";
        Double lat = 30.5003;
        Double lon = 40.2001;
        String cC = "ES";

        // Se instancia una ubicacion usando el constructor
        Ubicacion ubicacion = new Ubicacion(id, ubi, lat, lon, cC);

        // Probamos los getters
        assertEquals(ubicacion.getId(), id);
        assertEquals(ubicacion.getUbicacion(), ubi);
        assertEquals(ubicacion.getLat(), lat);
        assertEquals(ubicacion.getLon(), lon);
        assertEquals(ubicacion.getCountryCode(), cC);

        // La bandera de "favorita" deberia estar a false
        assertEquals(ubicacion.getBanderaUbiFav(), false);
    }

    @Test
    public void shouldCreateInstance2(){
        // Se definen los valores de los atributos
        int id = 999;
        String ubi = "Madrid";
        Double lat = 30.5003;
        Double lon = 40.2001;
        String cC = "ES";
        Boolean fav = false;

        // Se instancia una ubicacion usando el constructor
        Ubicacion ubicacion = new Ubicacion(id, ubi, lat, lon, fav, cC);

        // Probamos los getters
        assertEquals(ubicacion.getId(), id);
        assertEquals(ubicacion.getUbicacion(), ubi);
        assertEquals(ubicacion.getLat(), lat);
        assertEquals(ubicacion.getLon(), lon);
        assertEquals(ubicacion.getCountryCode(), cC);

        // La bandera de "favorita" deberia estar a false
        assertEquals(ubicacion.getBanderaUbiFav(), false);
    }
    @Test
    public void shouldUbicacionSettersGetters(){
        // Se crea un nuevo objeto ubicacion
        Ubicacion ubi = new Ubicacion(0, "", 0.0, 0.0, "");

        // Se definen los valores de los atributos que se van a asignar
        int id = 999;
        String cN = "Madrid";
        Double lat = 30.5003;
        Double lon = 40.2001;
        String cC = "ES";
        Boolean fav = false;

        //Se le asignan los atributos usando los setters
        ubi.setId(id);
        ubi.setUbicacion(cN);
        ubi.setLat(lat);
        ubi.setLon(lon);
        ubi.setCountryCode(cC);
        ubi.setBanderaUbiFav(fav);

        //Probamos los getters segun los setters
        assertEquals(id, ubi.getId());
        assertEquals(cN, ubi.getUbicacion());
        assertEquals(lat, ubi.getLat());
        assertEquals(lon, ubi.getLon());
        assertEquals(cC, ubi.getCountryCode());
        assertEquals(fav, ubi.getBanderaUbiFav());
    }

    @Test
    public void shouldUbicacionesDAO() {
        Ubicacion u1 = ubicacionDao.getUbicacion(123);
        assertEquals(u1,null);

        Ubicacion ubicacion = new Ubicacion(123, "test", 0.0, 0.0, false, "nc");
        ubicacionDao.insert(ubicacion);

        Ubicacion u2 = ubicacionDao.getUbicacion(123);
        assertEquals(u2.getId(),123);

    }

}

