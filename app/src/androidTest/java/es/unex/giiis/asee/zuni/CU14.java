package es.unex.giiis.asee.zuni;
import android.view.Gravity;

import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.DrawerMatchers;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/* ============================================================================================== */
/* CU14 : Consultar previsiones diarias por ubicacion =========================================== */
/* ============================================================================================== */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CU14 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldGetDailyReports() throws InterruptedException {

        /* NAVEGACION --------------------------------------------------------------------------- */

        // Abrir drawer
        onView(withId(R.id.drawer_layout)).check(matches(DrawerMatchers.isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open(), closeSoftKeyboard());
        // Navegar a previsiones por dia
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_previsiones), closeSoftKeyboard());
        // Pausa para cargar la vista
        Thread.sleep(500);

        /* PANTALLA PREVISIONES ----------------------------------------------------------------- */

        // Hacer click en el boton de buscar por ubicacion
        onView(withId(R.id.button)).perform(click(), closeSoftKeyboard());
        Thread.sleep(2000);

        // Comprobar si hay algun elemento en la lista
        onView(withId(R.id.listDaily)).check(matches(hasDescendant(withId(R.id.card_view))));

    }
}
