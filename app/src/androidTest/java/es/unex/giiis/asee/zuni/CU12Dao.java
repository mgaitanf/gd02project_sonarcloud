package es.unex.giiis.asee.zuni;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import es.unex.giiis.asee.zuni.eventos.Evento;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import es.unex.giiis.asee.zuni.eventos.db.EventoDao;
import es.unex.giiis.asee.zuni.eventos.db.EventoDatabase;

@RunWith(AndroidJUnit4.class)
public class CU12Dao {

    private EventoDao eventoDao;
    private EventoDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db =Room.inMemoryDatabaseBuilder(context, EventoDatabase.class).build();
        eventoDao = db.getDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void shouldDeleteDao() throws Exception {

        Date fecha= new Date(120, 5,3); //03/05/2020

        //Primera parte, crear Evento con datos
        Evento evento = new Evento(999, "Titulo", "Desc", fecha, Evento.Alerta.ALTA, "Madrid", 40.9, 40.1);

        List<Evento> eventoList = eventoDao.getAllEventos();
        int numeroAnterior = eventoList.size(); //Numero de elementos antes de insertar

        eventoDao.insert(evento);

        eventoList = eventoDao.getAllEventos();
        int numeroDespues = eventoList.size(); //Numero de elementos despues de insertar

        assertEquals(numeroAnterior+1,numeroDespues);

        // Desarrollador Junior

        // Eliminar el evento insertado anteriormente
        eventoDao.delete(evento.getId());

        // Comprobar que ha decrementado el numero de eventos
        eventoList = eventoDao.getAllEventos();
        numeroDespues = eventoList.size();
        assertEquals(numeroAnterior, numeroDespues);

        // Comprobamos que el evento anterior ya no existe en la BD
        Evento eventoBuscado = eventoDao.getEvento(evento.getId());
        assertEquals(eventoBuscado, null);
    }
}
