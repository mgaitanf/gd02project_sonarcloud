package es.unex.giiis.asee.zuni;

import android.view.Gravity;

import androidx.test.espresso.ViewAction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.DrawerMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.anything;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CU05 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldGetHourlyForecast() {


        /*/// IR AL FRAGMENT USANDO LA NAVEGACION
        //Abrir drawer
        onView(withId(R.id.drawer_layout)).check(matches(DrawerMatchers.isClosed(Gravity.LEFT))).perform(DrawerActions.open());
        //Navegar a*/

        //Asumimos que ya se está en la pantalla deseada, ya que esta es la pantalla principal de la MainActivity
        //Partimos también del hecho de que hay al menos 2 ubicaciones insertadas en la app


        /****SELECCIONAR OTRO ELEMENTO DEL SPINNER DE UBICACIONES****/
        //Abrir Spinner
        onView(withId(R.id.spinner_)).perform(click());

        //Seleccionar el segundo elemento
        onData(anything()).atPosition(1).perform(click());


        /****HACER BUSQUEDA****/

        //Hacer click en el boton de buscar por ubicacion
        onView(withId(R.id.button_)).perform(click(), ViewActions.closeSoftKeyboard());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ignored) {}

        //Comprobar si hay algun elemento en la lista
        onView(withId(R.id.list_items_)).check(matches(hasDescendant(withId(R.id.card_view))));
    }

}
