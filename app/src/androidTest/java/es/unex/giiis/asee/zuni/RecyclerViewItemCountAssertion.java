package es.unex.giiis.asee.zuni;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.ViewAssertion;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class RecyclerViewItemCountAssertion implements ViewAssertion {
    private int expectedCount;
    public int count;
    private boolean b;
    public RecyclerViewItemCountAssertion(int expectedCount) {
        this.expectedCount = expectedCount;
        count = 0;
        b = false;
    }

    public void setExpected (int expectedCount){
        this.expectedCount = expectedCount;
    }

    @Override
    public void check(View view, NoMatchingViewException noViewFoundException) {
        if (noViewFoundException != null) {
            throw noViewFoundException;
        }
        RecyclerView recyclerView = (RecyclerView) view;
        RecyclerView.Adapter adapter = recyclerView.getAdapter();

        if(b){
            // Segunda y tercera vez
            count = adapter.getItemCount();
            assertThat(adapter.getItemCount(), is(expectedCount));
        }
        else{
            // Primera vez
            count = adapter.getItemCount();
            b = true;
        }
    }
}